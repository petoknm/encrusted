<img src="https://demille.github.io/encrusted/src/img/name.svg" alt="encrusted" width="200px;"/>

---

#### A z-machine (interpreter) for Infocom-era text adventure games like Zork

Plug in a UI and you're ready to rock!

🎮 [Load the web version][web]

**Features**
- [x] Live mapping to keep track of where you are
- [x] Undo / Redo support
- [x] Object tree inspector

[web]: https://sterlingdemille.com/encrusted

### Install
Terminal version:

```sh
cargo install encrusted
```

Run a file with `encrusted <FILE>`.  
Use `$undo` and `$redo` to step through your move history.  
Use `save` and `restore` to save your progress.

### Tests
[![Build Status](https://travis-ci.org/DeMille/encrusted.svg?branch=master)](https://travis-ci.org/DeMille/encrusted)

Crude testing by running the [czech unit tests](https://inform-fiction.org/zmachine/standards/z1point1/appc.html):
```
cargo test
```

### Notes
- Currently only supports v3 zcode files
- Saves games in the Quetzal format

### License
MIT
