extern crate encrusted;

use std::str::FromStr;
use std::fs::File;
use std::env::args;
use std::io::{Read, stdin};

use encrusted::*;

struct TerminalUi;

impl Out for TerminalUi {
    fn output(&mut self, output: Output) {
        match output {
            Output::Text(t) => print!("{}", t),
            Output::Object(o) => print!("{}", o),
            _ => (),
        }
    }
}

fn main() {
    let mut data = Vec::new();
    let mut file = File::open(args().nth(1).unwrap()).unwrap();
    file.read_to_end(&mut data).unwrap();

    let mut zvm = Zmachine::new(data);
    loop {
        let mut input = String::new();
        let input = stdin()
            .read_line(&mut input)
            .map_err(|_| ())
            .and_then(|_| Input::from_str(&input))
            .ok();
        zvm.run(input, &mut TerminalUi);
    }
}
