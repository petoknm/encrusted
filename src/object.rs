use std::rc::Rc;

#[derive(Debug)]
pub struct Object {
    number: u16,
    name: String,
    children: Vec<Rc<Object>>,
}

impl Object {
    pub fn null() -> Object {
        Object { number: 0, name: "(Null Object)".to_string(), children: vec![] }
    }

    pub fn new(number: u16, name: String) -> Object {
        let name = match (number, &name[..]) {
            (0, _) => "(Null Object)",
            (_, "") => "(No Name)",
            (_, name) => name
        }.to_string();

        Object {
            number,
            name,
            children: Vec::new(),
        }
    }

    pub fn create_child(&mut self, number: u16, name: String) -> Rc<Object> {
        let o = Rc::new(Object::new(number, name));
        self.add_child(o.clone());
        o
    }

    pub fn add_child(&mut self, c: Rc<Object>) {
        self.children.push(c);
    }
}

#[derive(Debug)]
pub struct ObjectProperty {
    pub num: u8,
    pub len: u8,
    pub addr: usize,
    pub next: usize,
}

impl ObjectProperty {
    pub fn zero() -> ObjectProperty {
        ObjectProperty {
            num: 0,
            addr: 0,
            len: 0,
            next: 0,
        }
    }
}
