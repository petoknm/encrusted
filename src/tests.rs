use super::*;
use std::fs::read;

fn czech_this_out(data: Vec<u8>) -> String {
    let mut ui = &mut String::new();
    {
        let mut zvm = Zmachine::new(data);
        zvm.run(None, ui);
    }
    ui.clone()
}

fn czech(in_path: &str, out_path: &str) {
    let out = czech_this_out(read(in_path).unwrap());
    let expected = &read(out_path).unwrap();
    let expected = String::from_utf8_lossy(expected);
    assert_eq!(out.trim(), expected.trim());
}

macro_rules! czech {
    ($fn:ident, $v:expr) => {
        #[test]
        fn $fn(){
            czech(&format!("./tests/build/czech.z{}", $v), &format!("./tests/expected/expected.z{}.txt", $v));
        }
    }
}

czech!(z3, 3);
czech!(z4, 4);
czech!(z5, 5);
czech!(z8, 8);
