use std::str::FromStr;
use std::fmt::{Display, Error, Formatter, Write};

pub enum Cmd {
    Help, Dump, Dict, Tree, Room, You, Find, Object, Parent, Attrs,
    Props, Simple, Header, History, HaveAttr, HaveProp, Steal,
    Teleport, Quit, Undo, Redo
}

pub enum Input {
    Text(String),
    Cmd(Cmd),
}

impl FromStr for Input {
    type Err = ();

    fn from_str(s: &str) -> Result<Input, ()> {
        use self::{Input::*, Cmd::*};
        Ok(match s.trim() {
            "$help" => Cmd(Help),
            "$dump" => Cmd(Dump),
            "$dict" => Cmd(Dict),
            "$tree" => Cmd(Tree),
            "$room" => Cmd(Room),
            "$you" => Cmd(You),
            "$find" => Cmd(Find),
            "$object" => Cmd(Object),
            "$parent" => Cmd(Parent),
            "$attrs" => Cmd(Attrs),
            "$props" => Cmd(Props),
            "$simple" => Cmd(Simple),
            "$header" => Cmd(Header),
            "$history" => Cmd(History),
            "$have_attr" => Cmd(HaveAttr),
            "$have_prop" => Cmd(HaveProp),
            "$steal" => Cmd(Steal),
            "$teleport" => Cmd(Teleport),
            "$quit" => Cmd(Quit),
            "$redo" => Cmd(Redo),
            "$undo" => Cmd(Undo),
            _ => Text(s.to_string()),
        })
    }
}

#[derive(Clone)]
pub enum Output {
    Text(String),
    Object(String),
    StatusBar(String, String),
    SaveData(Vec<u8>),
}

impl Output {
    pub fn is_status_bar(&self) -> bool {
        match self {
            &Output::StatusBar(_, _) => true,
            _ => false,
        }
    }
}

impl Display for Output {
    fn fmt(&self, f: &mut Formatter) -> Result<(), Error> {
        write!(f, "{}", match self {
            Output::Text(t) => t,
            Output::Object(o) => o,
            _ => "",
        })
    }
}

pub trait Out {
    fn output(&mut self, output: Output);
}

impl Out for String {
    fn output(&mut self, output: Output) {
        write!(self, "{}", output).unwrap();
    }
}

impl Out for Vec<Output> {
    fn output(&mut self, output: Output) {
        self.push(output);
    }
}
